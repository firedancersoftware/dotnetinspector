///-----------------------------------------------------------------
///
/// @file      dotNetInspectorFrm.cpp
/// @author    Firedancer Software
/// Created:   6/05/2012 1:48:37 PM
/// @section   DESCRIPTION
///            dotNetInspectorFrm class implementation
///
///------------------------------------------------------------------

#include "dotNetInspectorFrm.h"
#include "Objects/MingW/dotNetInspector_private.h"
#include "CheckForUpdatesThread.h"
#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <wx/aboutdlg.h>
#include <wx/filedlg.h>
#include <wx/file.h>
#include <wx/textfile.h>
#include <wx/tokenzr.h>
#include <wx/clipbrd.h>
#include <wx/utils.h>
#include <wx/timer.h>
//#include <strsafe.h>
//#include <mscoree.h>

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End


// In case the machine this is compiled on does not have the most recent platform SDK
// with these values defined, define them here
#ifndef SM_TABLETPC
	#define SM_TABLETPC     86
#endif

#ifndef SM_MEDIACENTER
	#define SM_MEDIACENTER  87
#endif

#define CountOf(x) sizeof(x)/sizeof(*x)


// Constants that represent registry key names and value names
// to use for detection
const TCHAR *g_szNetfx10RegKeyName = _T("Software\\Microsoft\\.NETFramework\\Policy\\v1.0");
const TCHAR *g_szNetfx10RegKeyValue = _T("3705");
const TCHAR *g_szNetfx10SPxMSIRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{78705f0d-e8db-4b2d-8193-982bdda15ecd}");
const TCHAR *g_szNetfx10SPxOCMRegKeyName = _T("Software\\Microsoft\\Active Setup\\Installed Components\\{FDC11A6F-17D1-48f9-9EA3-9051954BAA24}");
const TCHAR *g_szNetfx11RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v1.1.4322");
const TCHAR *g_szNetfx20RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v2.0.50727");
const TCHAR *g_szNetfx30RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup");
const TCHAR *g_szNetfx30SpRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0");
const TCHAR *g_szNetfx30RegValueName = _T("InstallSuccess");
const TCHAR *g_szNetfx35RegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v3.5");
const TCHAR *g_szNetfx40ClientRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
const TCHAR *g_szNetfx40FullRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR *g_szNetfx40SPxRegValueName = _T("Servicing");
const TCHAR *g_szNetfx45ClientRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client");
const TCHAR *g_szNetfx45FullRegKeyName = _T("Software\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full");
const TCHAR *g_szNetfx45SPxRegValueName = _T("Servicing");
const TCHAR *g_szNetfx45RegValueName = _T("Release");
const TCHAR *g_szNetfxStandardRegValueName = _T("Install");
const TCHAR *g_szNetfxStandardSPxRegValueName = _T("SP");
const TCHAR *g_szNetfxStandardVersionRegValueName = _T("Version");

// Version information for final release of .NET Framework 3.0
const int g_iNetfx30VersionMajor = 3;
const int g_iNetfx30VersionMinor = 0;
const int g_iNetfx30VersionBuild = 4506;
const int g_iNetfx30VersionRevision = 26;

// Version information for final release of .NET Framework 3.5
const int g_iNetfx35VersionMajor = 3;
const int g_iNetfx35VersionMinor = 5;
const int g_iNetfx35VersionBuild = 21022;
const int g_iNetfx35VersionRevision = 8;

// Version information for final release of .NET Framework 4
const int g_iNetfx40VersionMajor = 4;
const int g_iNetfx40VersionMinor = 0;
const int g_iNetfx40VersionBuild = 30319;
const int g_iNetfx40VersionRevision = 0;

// Version information for final release of .NET Framework 4.5
const int g_iNetfx45VersionMajor = 4;
const int g_iNetfx45VersionMinor = 5;
const int g_iNetfx45VersionBuild = 50709;
const int g_iNetfx45VersionRevision = 0;
//	Actual Version currently reported in registry features only Major.Minor.Build
//	Revision should be set to zero (at least for now) to produce a valid match
//const int g_iNetfx45VersionRevision = 17929;

// Constants for known .NET Framework versions used with the GetRequestedRuntimeInfo API
const TCHAR *g_szNetfx10VersionString = _T("v1.0.3705");
const TCHAR *g_szNetfx11VersionString = _T("v1.1.4322");
const TCHAR *g_szNetfx20VersionString = _T("v2.0.50727");
const TCHAR *g_szNetfx40VersionString = _T("v4.0.30319");
const TCHAR *g_szNetfx45VersionString = _T("v4.5.50709");


//----------------------------------------------------------------------------
// dotNetInspectorFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(dotNetInspectorFrm,wxFrame)
	////Manual Code Start
	////Manual Code End

	EVT_CLOSE(dotNetInspectorFrm::OnClose)
	EVT_COMMAND(wxID_ANY, wxEVT_CHECKFORUPDATESTHREAD, dotNetInspectorFrm::OnCheckUpdateThread)
//	EVT_BUTTON(ID_WXCOPYTEXTBUTTON, dotNetInspectorFrm::WxButtonCopyTextClick)
//	EVT_BUTTON(ID_WXCOPYHTMLBUTTON, dotNetInspectorFrm::WxButtonCopyHtmlClick)
	EVT_MENU(wxID_SAVE, dotNetInspectorFrm::MnusaveClick)
	EVT_MENU(wxID_EXIT, dotNetInspectorFrm::MnuexitClick)
	EVT_MENU(ID_MNU_COPYTEXT, dotNetInspectorFrm::MnucopyTextClick)
	EVT_MENU(ID_MNU_COPYHTML, dotNetInspectorFrm::MnucopyHtmlClick)
	EVT_MENU(ID_MNU_CHECKFORUPDATES, dotNetInspectorFrm::MnucheckforupdatesClick)
	EVT_MENU(ID_MNU_WEBSITE, dotNetInspectorFrm::MnuwebsiteClick)
	EVT_MENU(wxID_ABOUT, dotNetInspectorFrm::MnuaboutClick)
    EVT_TIMER(wxEVT_TIMER, dotNetInspectorFrm::OnTimer)
END_EVENT_TABLE()
////Event Table End
dotNetInspectorFrm::dotNetInspectorFrm(wxString defaultExportPath, wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	exportPath = defaultExportPath;
	CreateGUIControls();

	bIsCheckingUpdates = false;
	CheckForUpdates(true);

	CheckFrameworkVersions();

static const int INTERVAL = 5000; // milliseconds
m_timer = new wxTimer(this, wxEVT_TIMER);
m_timer->Start(INTERVAL);
}

dotNetInspectorFrm::~dotNetInspectorFrm()
{
}

void dotNetInspectorFrm::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	this->SetSizer(WxBoxSizer1);
	this->SetAutoLayout(true);

	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(0, 0), wxDefaultSize);
	WxBoxSizer1->Add(WxPanel1, 1, wxALIGN_CENTER | wxALL, 0);

	WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
	WxPanel1->SetSizer(WxBoxSizer2);
	WxPanel1->SetAutoLayout(true);

	WxPanel3 = new wxPanel(WxPanel1, ID_WXPANEL3, wxPoint(0, 10), wxDefaultSize);
	WxBoxSizer2->Add(WxPanel3, 1, wxALIGN_CENTER | wxALL, 30);

	WxFlexGridSizer1 = new wxFlexGridSizer(8, 2, 0, 0);
	WxPanel3->SetSizer(WxFlexGridSizer1);
	WxPanel3->SetAutoLayout(true);

	WxStaticText1 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT1, _(".NET Framework 1.0"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText1"));
	WxFlexGridSizer1->Add(WxStaticText1, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx10Label = new wxStaticText(WxPanel3, ID_WX10LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx10Label"));
	WxFlexGridSizer1->Add(Wx10Label, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText2 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT2, _(".NET Framework 1.1"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText2"));
	WxFlexGridSizer1->Add(WxStaticText2, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx11Label = new wxStaticText(WxPanel3, ID_WX11LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx11Label"));
	WxFlexGridSizer1->Add(Wx11Label, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText3 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT3, _(".NET Framework 2.0"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText3"));
	WxFlexGridSizer1->Add(WxStaticText3, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx20Label = new wxStaticText(WxPanel3, ID_WX20LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx20Label"));
	WxFlexGridSizer1->Add(Wx20Label, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText4 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT4, _(".NET Framework 3.0"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText4"));
	WxFlexGridSizer1->Add(WxStaticText4, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx30Label = new wxStaticText(WxPanel3, ID_WX30LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx30Label"));
	WxFlexGridSizer1->Add(Wx30Label, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText5 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT5, _(".NET Framework 3.5"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText5"));
	WxFlexGridSizer1->Add(WxStaticText5, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx35Label = new wxStaticText(WxPanel3, ID_WX35LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx35Label"));
	WxFlexGridSizer1->Add(Wx35Label, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText6 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT6, _(".NET Framework 4.0 Client"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText6"));
	WxFlexGridSizer1->Add(WxStaticText6, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx40cLabel = new wxStaticText(WxPanel3, ID_WX40CLABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx40cLabel"));
	WxFlexGridSizer1->Add(Wx40cLabel, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText7 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT7, _(".NET Framework 4.0 Full"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText7"));
	WxFlexGridSizer1->Add(WxStaticText7, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx40fLabel = new wxStaticText(WxPanel3, ID_WX40FLABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx40fLabel"));
	WxFlexGridSizer1->Add(Wx40fLabel, 1, wxALIGN_RIGHT | wxALL, 5);

	WxStaticText8 = new wxStaticText(WxPanel3, ID_WXSTATICTEXT8, _(".NET Framework 4.5"), wxDefaultPosition, wxDefaultSize, 0, _("WxStaticText8"));
	WxFlexGridSizer1->Add(WxStaticText8, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	Wx45Label = new wxStaticText(WxPanel3, ID_WX45LABEL, _("not installed"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT, _("Wx45Label"));
	WxFlexGridSizer1->Add(Wx45Label, 1, wxALIGN_RIGHT | wxALL, 5);

/*	WxPanel4 = new wxPanel(WxPanel1, ID_WXPANEL4, wxDefaultPosition, wxDefaultSize);
	WxBoxSizer2->Add(WxPanel4, 0, wxALIGN_CENTER | wxLEFT | wxRIGHT | wxBOTTOM | wxTOP | wxEXPAND, 20);

	WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel4->SetSizer(WxBoxSizer4);
	WxPanel4->SetAutoLayout(true);

//	WxMoreButton = new wxButton(WxPanel4, ID_WXMOREBUTTON, _("Defaults >>"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _("WxMoreButton"));
//	WxBoxSizer4->Add(WxMoreButton, 0, wxALIGN_RIGHT | wxEXPAND | wxALL, 10);
//	WxBoxSizer4->AddStretchSpacer();
	WxCopyTextButton = new wxButton(WxPanel4, ID_WXCOPYTEXTBUTTON, _("Copy as Text"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _("WxCopyToClipboardButton"));
	WxBoxSizer4->Add(WxCopyTextButton, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	WxBoxSizer4->AddStretchSpacer();

	WxCopyHtmlButton = new wxButton(WxPanel4, ID_WXCOPYHTMLBUTTON, _("Copy as HTML"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _("WxHtmlToClipboardButton"));
	WxBoxSizer4->Add(WxCopyHtmlButton, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);
*/
	WxMenuBar1 = new wxMenuBar();
	wxMenu *ID_MNU_FILE_Mnu_Obj = new wxMenu();
	ID_MNU_FILE_Mnu_Obj->Append(wxID_SAVE, "", _(""), wxITEM_NORMAL);
	ID_MNU_FILE_Mnu_Obj->AppendSeparator();
	ID_MNU_FILE_Mnu_Obj->Append(wxID_EXIT, _("E&xit"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_FILE_Mnu_Obj, _("&File"));

	wxMenu *ID_MNU_COPY_Mnu_Obj = new wxMenu();
	ID_MNU_COPY_Mnu_Obj->Append(ID_MNU_COPYTEXT, _("&Copy as Text\tCtrl+C"), _(""), wxITEM_NORMAL);
	ID_MNU_COPY_Mnu_Obj->Append(ID_MNU_COPYHTML, _("Copy as &HTML\tCtrl+H"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_COPY_Mnu_Obj, _("&Edit"));

	wxMenu *ID_MNU_HELP_Mnu_Obj = new wxMenu();
	ID_MNU_HELP_Mnu_Obj->Append(ID_MNU_CHECKFORUPDATES, _("&Check For Updates"));
	ID_MNU_HELP_Mnu_Obj->AppendSeparator();
	ID_MNU_HELP_Mnu_Obj->Append(ID_MNU_WEBSITE, _("&Website"));
	ID_MNU_HELP_Mnu_Obj->Append(wxID_ABOUT, _("&About"));
	WxMenuBar1->Append(ID_MNU_HELP_Mnu_Obj, _("&Help"));
	SetMenuBar(WxMenuBar1);

	this->SetBackgroundColour(wxColour(_("WHITE")));


	SetTitle(_("dotNETInspector"));
	Layout();
	GetSizer()->Fit(this);
	GetSizer()->SetSizeHints(this);
	Center();

	////GUI Items Creation End
}

void dotNetInspectorFrm::OnClose(wxCloseEvent& event)
{
	Destroy();
}

void dotNetInspectorFrm::OnTimer(wxTimerEvent& event)
{
	CheckFrameworkVersions();
}


/*
 * MnusaveClick
 */
void dotNetInspectorFrm::MnusaveClick(wxCommandEvent& event)
{
	SaveToFile();
}


/*
 * MnuexitClick
 */
void dotNetInspectorFrm::MnuexitClick(wxCommandEvent& event)
{
	Close(true);
}


/*
 * MnucheckforupdatesClick
 */
void dotNetInspectorFrm::MnucheckforupdatesClick(wxCommandEvent& event)
{
	CheckForUpdates(false);
}


/*
 * MnuwebsiteClick
 */
void dotNetInspectorFrm::MnuwebsiteClick(wxCommandEvent& event)
{
	wxLaunchDefaultBrowser(wxT("http://firedancer-software.com/software/dotnetinspector/"));
}


/*
 * MnuaboutClick
 */
void dotNetInspectorFrm::MnuaboutClick(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(PRODUCT_NAME);
    info.SetVersion(PRODUCT_VERSION);
    info.SetCopyright(wxT("(C) 2013 Firedancer Software.\nLicenced under the GPL Version 3.\n"));
    info.SetDescription(wxT("Determine which versions of the .NET Framework are installed."));
	wxAboutBox(info);
}


void dotNetInspectorFrm::MnucopyTextClick(wxCommandEvent& event)
{
	CopyToClipboard(GetFormattedResults());
}


void dotNetInspectorFrm::MnucopyHtmlClick(wxCommandEvent& event)
{
	CopyToClipboard(GetHtmlResults());
}

/*
void dotNetInspectorFrm::WxButtonCopyTextClick(wxCommandEvent& event)
{
	CopyToClipboard(GetFormattedResults());
}


void dotNetInspectorFrm::WxButtonCopyHtmlClick(wxCommandEvent& event)
{
	CopyToClipboard(GetHtmlResults());
}
*/

void dotNetInspectorFrm::CheckForUpdates(bool bCheckSilently)
{
	bIsCheckingUpdates = true;
	WxMenuBar1->Enable(ID_MNU_CHECKFORUPDATES, false);
    CheckForUpdatesThread *thread = new CheckForUpdatesThread(this, bCheckSilently);
    thread->Create();
    thread->Run();
}


void dotNetInspectorFrm::OnCheckUpdateThread(wxCommandEvent& event)
{
	WxMenuBar1->Enable(ID_MNU_CHECKFORUPDATES, true);
	bIsCheckingUpdates = false;
	return;
}

void dotNetInspectorFrm::SaveToFile()
{
	wxDateTime now = wxDateTime::Now();
	wxString fileName = wxT("dotNETInspector");
	fileName << "-" << now.FormatISODate();
	wxFileDialog saveFileDialog(this, _("Save to file"), exportPath, fileName, "*.txt", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	if(saveFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}
	wxFile *output = new wxFile(saveFileDialog.GetPath(), wxFile::write);

	if(output->IsOpened())
	{
		wxString outStr = "";
		outStr << "dotNETInspector results - " << now.FormatISOTime() << " " << now.FormatDate() << wxTextFile::GetEOL();
		outStr << "Computer name: " << ::wxGetHostName() << wxTextFile::GetEOL();
		outStr << wxTextFile::GetEOL();
		outStr << GetFormattedResults();
		output->Write(outStr);
		output->Close();
	}
}


wxString dotNetInspectorFrm::GetFormattedResults()
{
	wxString outStr = "";
	outStr << ".NET Framework 1.0         " << netFx10Result << wxTextFile::GetEOL();
	outStr << ".NET Framework 1.1         " << netFx11Result << wxTextFile::GetEOL();
	outStr << ".NET Framework 2.0         " << netFx20Result << wxTextFile::GetEOL();
	outStr << ".NET Framework 3.0         " << netFx30Result << wxTextFile::GetEOL();
	outStr << ".NET Framework 3.5         " << netFx35Result << wxTextFile::GetEOL();
	outStr << ".NET Framework 4.0 Client  " << netFx40ClientResult << wxTextFile::GetEOL();
	outStr << ".NET Framework 4.0 Full    " << netFx40FullResult << wxTextFile::GetEOL();
	outStr << ".NET Framework 4.5         " << netFx45Result << wxTextFile::GetEOL();
	return outStr;
}


wxString dotNetInspectorFrm::GetHtmlResults()
{
	wxString outStr = "";
	outStr << "<table>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 1.0</td><td>" << netFx10Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 1.1</td><td>" << netFx11Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 2.0</td><td>" << netFx20Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 3.0</td><td>" << netFx30Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 3.5</td><td>" << netFx35Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 4.0 Client</td><td>" << netFx40ClientResult << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 4.0 Full</td><td>" << netFx40FullResult << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "<tr><td>.NET Framework 4.5</td><td>" << netFx45Result << "</td></tr>" << wxTextFile::GetEOL();
	outStr << "</table>" << wxTextFile::GetEOL();
	return outStr;
}


void dotNetInspectorFrm::CopyToClipboard(wxString text)
{
	if (wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new wxTextDataObject(text));
		wxTheClipboard->Close();
	}
}


void dotNetInspectorFrm::CheckFrameworkVersions()
{
	bInstalled10 = IsNetfx10Installed();
	bInstalled11 = IsNetfx11Installed();
	bInstalled20 = IsNetfx20Installed();
	bInstalled30 = IsNetfx30Installed();
	bInstalled35 = IsNetfx35Installed();
	bInstalled40Client = IsNetfx40ClientInstalled();
	bInstalled40Full = IsNetfx40FullInstalled();
	bInstalled45 = IsNetfx45Installed();


	int majorVersion;
	int minorVersion;
	bool is64Bit = wxIsPlatform64Bit();

	wxGetOsVersion(&majorVersion, &minorVersion);
	OSVERSIONINFOEX osvi;
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	GetVersionEx((LPOSVERSIONINFO )&osvi);

	netFx10Result = "not installed";
	if (bInstalled10)
	{
		netFx10Sp = GetNetfx10SPLevel();

		if (netFx10Sp > 0)
		{
			netFx10Result = "";
			netFx10Result << "service pack " << netFx10Sp;
		}
		else
		{
			netFx10Result = "installed";
		}
	}
	Wx10Label->SetLabel(netFx10Result);

	netFx11Result = "not installed";
	if (bInstalled11)
	{
		netFx11Sp = GetNetfxSPLevel(g_szNetfx11RegKeyName, g_szNetfxStandardSPxRegValueName);

		if (netFx11Sp > 0)
		{
			netFx11Result = "";
			netFx11Result << "service pack " << netFx11Sp;
		}
		else
		{
			netFx11Result = "installed";
		}
		// If Windows Server 2003 32-bit (not R2) this version is pre-installed
/*		if(majorVersion == 5 && minorVersion == 2 && GetSystemMetrics(SM_SERVERR2) == 0 && !is64Bit)
		{
			temp << " (component)";
		}
*/	}
	Wx11Label->SetLabel(netFx11Result);

	netFx20Result = "not installed";
	if (bInstalled20)
	{
		netFx20Sp = GetNetfxSPLevel(g_szNetfx20RegKeyName, g_szNetfxStandardSPxRegValueName);
		wxString temp;

		if (netFx20Sp > 0)
		{
			netFx20Result = "";
			netFx20Result << "service pack " << netFx20Sp;
		}
		else
		{
			netFx20Result = "installed";
		}

		// If Windows Vista (all editions) this version is pre-installed
/*		if((majorVersion == 6 && minorVersion == 0 && osvi.wProductType == VER_NT_WORKSTATION)
		|| (majorVersion == 6 && minorVersion == 0)
		|| (majorVersion == 6 && minorVersion > 0))
		{
			temp << " (component)";
		}
*/	}
		Wx20Label->SetLabel(netFx20Result);

	netFx30Result = "not installed";
	if (bInstalled20 && bInstalled30)
	{
		netFx30Sp = GetNetfxSPLevel(g_szNetfx30SpRegKeyName, g_szNetfxStandardSPxRegValueName);
		wxString temp;

		if (netFx30Sp > 0)
		{
			netFx30Result = "";
			netFx30Result << "service pack " << netFx30Sp;
		}
		else
		{
			netFx30Result = "installed";
		}
		// If Windows Vista (all editions) this version is pre-installed
/*		if(majorVersion == 6 && minorVersion == 0 && osvi.wProductType == VER_NT_WORKSTATION && netFx30Sp == 0)
		{
//			Up to here - how to detect Vista SP1???
			temp << " (component)";
		}
*/
	}
	Wx30Label->SetLabel(netFx30Result);

	netFx35Result = "not installed";
	if (bInstalled20 && bInstalled30 && bInstalled35)
	{
		netFx35Sp = GetNetfxSPLevel(g_szNetfx35RegKeyName, g_szNetfxStandardSPxRegValueName);
		if (netFx35Sp > 0)
		{
			netFx35Result = "";
			netFx35Result << "service pack " << netFx35Sp;
		}
		else
		{
			netFx35Result = "installed";
		}
	}
	Wx35Label->SetLabel(netFx35Result);

	netFx40ClientResult = "not installed";
	if (bInstalled40Client)
	{
		netFx40ClientSp = GetNetfxSPLevel(g_szNetfx40ClientRegKeyName, g_szNetfx40SPxRegValueName);
		if (netFx40ClientSp > 0)
		{
			netFx40ClientResult = "";
			netFx40ClientResult << "service pack " << netFx40ClientSp;
		}
		else
		{
			netFx40ClientResult = "installed";
		}
	}
	Wx40cLabel->SetLabel(netFx40ClientResult);

	netFx40FullResult = "not installed";
	if (bInstalled40Full)
	{
		netFx40FullSp = GetNetfxSPLevel(g_szNetfx40FullRegKeyName, g_szNetfx40SPxRegValueName);
		if (netFx40FullSp > 0)
		{
			netFx40FullResult = "";
			netFx40FullResult << "service pack " << netFx40FullSp;
		}
		else
		{
			netFx40FullResult = "installed";
		}
	}
	Wx40fLabel->SetLabel(netFx40FullResult);


	// .NET Framework 4.5 requires Windows Vista as a minimum.
	netFx45Result = "not supported";
	if(wxPlatformInfo::Get().CheckOSVersion(6, 0))
	{
		if ((bInstalled40Client || bInstalled40Full) && bInstalled45)
		{
			wxString Netfx45TargetKey = "";

			if(bInstalled40Client)
			{
				Netfx45TargetKey = g_szNetfx45FullRegKeyName;
			}
			else
			{
				Netfx45TargetKey = g_szNetfx45ClientRegKeyName;
			}

			netFx45Sp = GetNetfxSPLevel(Netfx45TargetKey, g_szNetfx45SPxRegValueName);
			if (netFx45Sp > 0)
			{
			netFx45Result = "";
			netFx45Result << "service pack " << netFx45Sp;
			}
			else
			{
				netFx45Result = "installed";
			}
		}
		else
		{
			netFx45Result = "not installed";
		}
	}
	Wx45Label->SetLabel(netFx45Result);

	// Recalculate sizer dimensions after setting labels
    GetSizer()->Fit(this);
}


/******************************************************************
Function Name:  CheckNetfxVersionUsingMscoree
Description:    Uses the logic described in the sample code at
                http://msdn2.microsoft.com/library/ydh6b3yb.aspx
                to load mscoree.dll and call its APIs to determine
                whether or not a specific version of the .NET
                Framework is installed on the system
Inputs:         pszNetfxVersionToCheck - version to look for
Results:        true if the requested version is installed
                false otherwise
******************************************************************/
/*bool dotNetInspectorFrm::CheckNetfxVersionUsingMscoree(const TCHAR *pszNetfxVersionToCheck)
{
	bool bFoundRequestedNetfxVersion = false;
	HRESULT hr = S_OK;

	// Check input parameter
	if (NULL == pszNetfxVersionToCheck)
		return false;

	HMODULE hmodMscoree = LoadLibraryEx(_T("mscoree.dll"), NULL, 0);
	if (NULL != hmodMscoree)
	{
		typedef HRESULT (STDAPICALLTYPE *GETCORVERSION)(LPWSTR szBuffer, DWORD cchBuffer, DWORD* dwLength);
		GETCORVERSION pfnGETCORVERSION = (GETCORVERSION)GetProcAddress(hmodMscoree, "GetCORVersion");

		// Some OSs shipped with a placeholder copy of mscoree.dll. The existence of mscoree.dll
		// therefore does NOT mean that a version of the .NET Framework is installed.
		// If this copy of mscoree.dll does not have an exported function named GetCORVersion
		// then we know it is a placeholder DLL.
		if (NULL == pfnGETCORVERSION)
			goto Finish;

		typedef HRESULT (STDAPICALLTYPE *CORBINDTORUNTIME)(LPCWSTR pwszVersion, LPCWSTR pwszBuildFlavor, REFCLSID rclsid, REFIID riid, LPVOID FAR *ppv);
		CORBINDTORUNTIME pfnCORBINDTORUNTIME = (CORBINDTORUNTIME)GetProcAddress(hmodMscoree, "CorBindToRuntime");

		typedef HRESULT (STDAPICALLTYPE *GETREQUESTEDRUNTIMEINFO)(LPCWSTR pExe, LPCWSTR pwszVersion, LPCWSTR pConfigurationFile, DWORD startupFlags, DWORD runtimeInfoFlags, LPWSTR pDirectory, DWORD dwDirectory, DWORD *dwDirectoryLength, LPWSTR pVersion, DWORD cchBuffer, DWORD* dwlength);
		GETREQUESTEDRUNTIMEINFO pfnGETREQUESTEDRUNTIMEINFO = (GETREQUESTEDRUNTIMEINFO)GetProcAddress(hmodMscoree, "GetRequestedRuntimeInfo");

		if (NULL != pfnCORBINDTORUNTIME)
		{
			WCHAR szRetrievedVersion[50];
			DWORD dwLength = CountOf(szRetrievedVersion);

			if (NULL == pfnGETREQUESTEDRUNTIMEINFO)
			{
				// Having CorBindToRuntimeHost but not having GetRequestedRuntimeInfo means that
				// this machine contains no higher than .NET Framework 1.0, but the only way to
				// 100% guarantee that the .NET Framework 1.0 is installed is to call a function
				// to exercise its functionality
				if (0 == _tcscmp(pszNetfxVersionToCheck, g_szNetfx10VersionString))
				{
					hr = pfnGETCORVERSION(szRetrievedVersion, dwLength, &dwLength);

					if (SUCCEEDED(hr))
					{
						if (0 == _tcscmp(szRetrievedVersion, g_szNetfx10VersionString))
							bFoundRequestedNetfxVersion = true;
					}

					goto Finish;
				}
			}

			// Set error mode to prevent the .NET Framework from displaying
			// unfriendly error dialogs
			UINT uOldErrorMode = SetErrorMode(SEM_FAILCRITICALERRORS);

			TCHAR szDirectory[MAX_PATH];
			DWORD dwDirectoryLength = 0;
//			DWORD dwRuntimeInfoFlags = RUNTIME_INFO_DONT_RETURN_DIRECTORY | GetProcessorArchitectureFlag();
			DWORD dwRuntimeInfoFlags = GetProcessorArchitectureFlag();

			// Check for the requested .NET Framework version
			hr = pfnGETREQUESTEDRUNTIMEINFO(NULL, pszNetfxVersionToCheck, NULL, STARTUP_LOADER_OPTIMIZATION_MULTI_DOMAIN_HOST, NULL, szDirectory, CountOf(szDirectory), &dwDirectoryLength, szRetrievedVersion, CountOf(szRetrievedVersion), &dwLength);

			if (SUCCEEDED(hr))
				bFoundRequestedNetfxVersion = true;

			// Restore the previous error mode
			SetErrorMode(uOldErrorMode);
		}
	}

Finish:
	if (hmodMscoree)
	{
		FreeLibrary(hmodMscoree);
	}

	return bFoundRequestedNetfxVersion;
}

*/
/******************************************************************
Function Name:  GetNetfx10SPLevel
Description:    Uses the detection method recommended at
                http://blogs.msdn.com/astebner/archive/2004/09/14/229802.aspx
                to determine what service pack for the
                .NET Framework 1.0 is installed on the machine
Inputs:         NONE
Results:        integer representing SP level for .NET Framework 1.0
******************************************************************/
int dotNetInspectorFrm::GetNetfx10SPLevel()
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR *pszSPLevel = NULL;
	int iRetValue = -1;
	bool bRegistryRetVal = false;

	// Need to detect what OS we are running on so we know what
	// registry key to use to look up the SP level
	if (IsCurrentOSTabletMedCenter())
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxOCMRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);
	else
		bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10SPxMSIRegKeyName, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);

	if (bRegistryRetVal)
	{
		// This registry value should be of the format
		// #,#,#####,# where the last # is the SP level
		// Try to parse off the last # here
		pszSPLevel = _tcsrchr(szRegValue, _T(','));
		if (NULL != pszSPLevel)
		{
			// Increment the pointer to skip the comma
			pszSPLevel++;

			// Convert the remaining value to an integer
			iRetValue = wxAtoi(pszSPLevel);
		}
	}

	return iRetValue;
}


/******************************************************************
Function Name:	GetNetfxSPLevel
Description:	Determine what service pack is installed for a
                version of the .NET Framework using registry
				based detection methods documented in the
				.NET Framework deployment guides.
Inputs:         pszNetfxRegKeyName - registry key name to use for detection
				pszNetfxRegValueName - registry value to use for detection
Results:        integer representing SP level for .NET Framework
******************************************************************/
int dotNetInspectorFrm::GetNetfxSPLevel(const TCHAR *pszNetfxRegKeyName, const TCHAR *pszNetfxRegValueName)
{
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		return (int)dwRegValue;
	}

	// We can only get here if the .NET Framework is not
	// installed or there was some kind of error retrieving
	// the data from the registry
	return -1;
}


/******************************************************************
Function Name:  GetProcessorArchitectureFlag
Description:    Determine the processor architecture of the
                system (x86, x64, ia64)
Inputs:         NONE
Results:        DWORD processor architecture flag
******************************************************************/
DWORD dotNetInspectorFrm::GetProcessorArchitectureFlag()
{
	HMODULE hmodKernel32 = NULL;
	typedef void (WINAPI *PFnGetNativeSystemInfo) (LPSYSTEM_INFO);
	PFnGetNativeSystemInfo pfnGetNativeSystemInfo;

	SYSTEM_INFO sSystemInfo;
	memset(&sSystemInfo, 0, sizeof(sSystemInfo));

	bool bRetrievedSystemInfo = false;

	// Attempt to load kernel32.dll
	hmodKernel32 = LoadLibrary(_T("Kernel32.dll"));
	if (NULL != hmodKernel32)
	{
		// If the DLL loaded correctly, get the proc address for GetNativeSystemInfo
		pfnGetNativeSystemInfo = (PFnGetNativeSystemInfo) GetProcAddress(hmodKernel32, "GetNativeSystemInfo");
		if (NULL != pfnGetNativeSystemInfo)
		{
			// Call GetNativeSystemInfo if it exists
			(*pfnGetNativeSystemInfo)(&sSystemInfo);
			bRetrievedSystemInfo = true;
		}
		FreeLibrary(hmodKernel32);
	}

	if (!bRetrievedSystemInfo)
	{
		// Fallback to calling GetSystemInfo if the above failed
		GetSystemInfo(&sSystemInfo);
		bRetrievedSystemInfo = true;
	}

	if (bRetrievedSystemInfo)
	{
		switch (sSystemInfo.wProcessorArchitecture)
		{
			case PROCESSOR_ARCHITECTURE_INTEL:
				return RUNTIME_INFO_REQUEST_X86;
			case PROCESSOR_ARCHITECTURE_IA64:
				return RUNTIME_INFO_REQUEST_IA64;
			case PROCESSOR_ARCHITECTURE_AMD64:
				return RUNTIME_INFO_REQUEST_AMD64;
			default:
				return 0;
		}
	}

	return 0;
}


/******************************************************************
Function Name:	CheckNetfxBuildNumber
Description:	Retrieves the .NET Framework build number from
                the registry and validates that it is not a pre-release
                version number
Inputs:         NONE
Results:        true if the build number in the registry is greater
				than or equal to the passed in version; false otherwise
******************************************************************/
bool dotNetInspectorFrm::CheckNetfxBuildNumber(const TCHAR *pszNetfxRegKeyName, const TCHAR *pszNetfxRegKeyValue, const int iRequestedVersionMajor, const int iRequestedVersionMinor, const int iRequestedVersionBuild, const int iRequestedVersionRevision)
{
	TCHAR szRegValue[MAX_PATH];
	TCHAR *pszToken = NULL;
	TCHAR *pszNextToken = NULL;
	int iVersionPartCounter = 0;
	int iRegistryVersionMajor = 0;
	int iRegistryVersionMinor = 0;
	int iRegistryVersionBuild = 0;
	int iRegistryVersionRevision = 0;
	bool bRegistryRetVal = false;

	// Attempt to retrieve the build number registry value
	bRegistryRetVal = RegistryGetValue(HKEY_LOCAL_MACHINE, pszNetfxRegKeyName, pszNetfxRegKeyValue, NULL, (LPBYTE)szRegValue, MAX_PATH);

	if (bRegistryRetVal)
	{
		// This registry value should be of the format
		// #.#.#####.##.  Try to parse the 4 parts of
		// the version here
		wxString retVal;
		retVal << szRegValue;
		wxStringTokenizer tkz(retVal, wxT("."));

		while(tkz.HasMoreTokens())
		{
			wxString token = tkz.GetNextToken();
			iVersionPartCounter++;

			switch (iVersionPartCounter)
			{
			case 1:
				// Convert the major version value to an integer
				iRegistryVersionMajor = wxAtoi(token);
				break;
			case 2:
				// Convert the minor version value to an integer
				iRegistryVersionMinor = wxAtoi(token);
				break;
			case 3:
				// Convert the build number value to an integer
				iRegistryVersionBuild = wxAtoi(token);
				break;
			case 4:
				// Convert the revision number value to an integer
				iRegistryVersionRevision = wxAtoi(token);
				break;
			default:
				break;

			}
		}
	}

	// Compare the version number retrieved from the registry with
	// the version number of the final release of the .NET Framework
	// that we are checking
	if (iRegistryVersionMajor > iRequestedVersionMajor)
	{
		return true;
	}
	else if (iRegistryVersionMajor == iRequestedVersionMajor)
	{
		if (iRegistryVersionMinor > iRequestedVersionMinor)
		{
			return true;
		}
		else if (iRegistryVersionMinor == iRequestedVersionMinor)
		{
			if (iRegistryVersionBuild > iRequestedVersionBuild)
			{
				return true;
			}
			else if (iRegistryVersionBuild == iRequestedVersionBuild)
			{
				if (iRegistryVersionRevision >= iRequestedVersionRevision)
				{
					return true;
				}
			}
		}
	}

	// If we get here, the version in the registry must be less than the
	// version of the final release of the .NET Framework we are checking,
	// so return false
	return false;
}


/******************************************************************
Function Name:  IsCurrentOSTabletMedCenter
Description:    Determine if the current OS is a Windows XP
                Tablet PC Edition or Windows XP Media Center
                Edition system
Inputs:         NONE
Results:        true if the OS is Tablet PC or Media Center
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsCurrentOSTabletMedCenter()
{
	// Use GetSystemMetrics to detect if we are on a Tablet PC or Media Center OS
	return ( (GetSystemMetrics(SM_TABLETPC) != 0) || (GetSystemMetrics(SM_MEDIACENTER) != 0) );
}


/******************************************************************
Function Name:  IsNetfx10Installed
Description:    Uses the detection method recommended at
                http://msdn.microsoft.com/library/ms994349.aspx
                to determine whether the .NET Framework 1.0 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 1.0 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx10Installed()
{
	TCHAR szRegValue[MAX_PATH];
	return (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx10RegKeyName, g_szNetfx10RegKeyValue, NULL, (LPBYTE)szRegValue, MAX_PATH));
}


/******************************************************************
Function Name:  IsNetfx11Installed
Description:    Uses the detection method recommended at
                http://msdn.microsoft.com/library/ms994339.aspx
                to determine whether the .NET Framework 1.1 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 1.1 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx11Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx11RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}


/******************************************************************
Function Name:	IsNetfx20Installed
Description:	Uses the detection method recommended at
                http://msdn2.microsoft.com/library/aa480243.aspx
                to determine whether the .NET Framework 2.0 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 2.0 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx20Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx20RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	return bRetValue;
}


/******************************************************************
Function Name:	IsNetfx30Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/aa964979.aspx
                to determine whether the .NET Framework 3.0 is
                installed on the machine
Inputs:	        NONE
Results:        true if the .NET Framework 3.0 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx30Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	// Check that the InstallSuccess registry value exists and equals 1
	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx30RegKeyName, g_szNetfx30RegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 3.0 can
	// have the InstallSuccess value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx30RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx30VersionMajor, g_iNetfx30VersionMinor, g_iNetfx30VersionBuild, g_iNetfx30VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx35Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/cc160716.aspx
                to determine whether the .NET Framework 3.5 is
                installed on the machine
Inputs:	        NONE
Results:        true if the .NET Framework 3.5 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx35Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	// Check that the Install registry value exists and equals 1
	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx35RegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 3.5 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx35RegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx35VersionMajor, g_iNetfx35VersionMinor, g_iNetfx35VersionBuild, g_iNetfx35VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx40ClientInstalled
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/ee942965(v=VS.100).aspx
                to determine whether the .NET Framework 4 Client is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4 Client is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx40ClientInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40ClientRegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 4 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40ClientRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx40FullInstalled
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/library/ee942965(v=VS.100).aspx
                to determine whether the .NET Framework 4 Full is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4 Full is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx40FullInstalled()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, g_szNetfx40FullRegKeyName, g_szNetfxStandardRegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (1 == dwRegValue)
			bRetValue = true;
	}

	// A system with a pre-release version of the .NET Framework 4 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry
	return (bRetValue && CheckNetfxBuildNumber(g_szNetfx40FullRegKeyName, g_szNetfxStandardVersionRegValueName, g_iNetfx40VersionMajor, g_iNetfx40VersionMinor, g_iNetfx40VersionBuild, g_iNetfx40VersionRevision));
}


/******************************************************************
Function Name:	IsNetfx45Installed
Description:	Uses the detection method recommended at
                http://msdn.microsoft.com/en-us/library/ee942965.aspx
                to determine whether the .NET Framework 4.5 is
                installed on the machine
Inputs:         NONE
Results:        true if the .NET Framework 4.5 is installed
                false otherwise
******************************************************************/
bool dotNetInspectorFrm::IsNetfx45Installed()
{
	bool bRetValue = false;
	DWORD dwRegValue=0;
	wxString Netfx45TargetKey;
//	wxString output;

if(IsNetfx40FullInstalled())
{
	Netfx45TargetKey = g_szNetfx45FullRegKeyName;
//	output << ".NET 4.0 Full Installed";
}
else
{
	Netfx45TargetKey = g_szNetfx45ClientRegKeyName;
//	output << ".NET 4.0 Client Installed";
}

	if (RegistryGetValue(HKEY_LOCAL_MACHINE, Netfx45TargetKey, g_szNetfx45RegValueName, NULL, (LPBYTE)&dwRegValue, sizeof(DWORD)))
	{
		if (378389 <= dwRegValue)
			bRetValue = true;
	}

/*	output << "\nKey: " << Netfx45TargetKey;
	output << "\nValue: " << g_szNetfxStandardRegValueName;
	output << "\nInstalled: " << (bRetValue ? "true" : "false");
	output << "\n";
	output << "\nKey: " << Netfx45TargetKey;
	output << "\nValue: " << g_szNetfxStandardVersionRegValueName;
	output << "\nMajor: " << g_iNetfx45VersionMajor;
	output << "\nMinor: " << g_iNetfx45VersionMinor;
	output << "\nBuild: " << g_iNetfx45VersionBuild;
	output << "\nRevision: " << g_iNetfx45VersionRevision;
//	output << "\nFound: " << RegistryGetValue(HKEY_LOCAL_MACHINE, Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, NULL, (LPBYTE)szRegValue, MAX_PATH);
	output << "\nFound: " << CheckNetfxBuildNumber(Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, g_iNetfx45VersionMajor, g_iNetfx45VersionMinor, g_iNetfx45VersionBuild, g_iNetfx45VersionRevision);


	wxMessageBox(output);
*/	// A system with a pre-release version of the .NET Framework 4.5 can
	// have the Install value.  As an added verification, check the
	// version number listed in the registry

return bRetValue;
	// do not need to check for version number here, that check has already been performed above
//	return (bRetValue && CheckNetfxBuildNumber(Netfx45TargetKey, g_szNetfxStandardVersionRegValueName, g_iNetfx45VersionMajor, g_iNetfx45VersionMinor, g_iNetfx45VersionBuild, g_iNetfx45VersionRevision));
}


/******************************************************************
Function Name:  RegistryGetValue
Description:    Get the value of a reg key
Inputs:         HKEY hk - The hk of the key to retrieve
                TCHAR *pszKey - Name of the key to retrieve
                TCHAR *pszValue - The value that will be retrieved
                DWORD dwType - The type of the value that will be retrieved
                LPBYTE data - A buffer to save the retrieved data
                DWORD dwSize - The size of the data retrieved
Results:        true if successful, false otherwise
******************************************************************/
bool dotNetInspectorFrm::RegistryGetValue(HKEY hk, const TCHAR * pszKey, const TCHAR * pszValue, DWORD dwType, LPBYTE data, DWORD dwSize)
{
	HKEY hkOpened;

	// Try to open the key
	if (RegOpenKeyEx(hk, pszKey, 0, KEY_READ, &hkOpened) != ERROR_SUCCESS)
	{
		return false;
	}

	// If the key was opened, try to retrieve the value
	if (RegQueryValueEx(hkOpened, pszValue, 0, &dwType, (LPBYTE)data, &dwSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hkOpened);
		return false;
	}

	// Clean up
	RegCloseKey(hkOpened);

	return true;
}

