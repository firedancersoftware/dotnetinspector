//---------------------------------------------------------------------------
//
// Name:        dotNetInspectorApp.h
// Author:      Firedancer Software
// Created:     6/05/2012 1:48:36 PM
// Description:
//
//---------------------------------------------------------------------------

#ifndef __DOTNETINSPECTORFRMApp_h__
#define __DOTNETINSPECTORFRMApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/cmdline.h>

class dotNetInspectorFrmApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
		wxCmdLineParser parser;
		wxString defaultExportPath;
/*		wxCmdLineParser parser;
		bool bCheckVersion;
		wxString version;
		bool bCheckServicePack;
		long lServicePack;
		bool bExecuteIfFound;
		wxString executeIfFoundPath;
		bool bExecuteIfNotFound;
		wxString executeIfNotFoundPath;
*/
/*
	private:
		bool CheckNetfxBuildNumber(const TCHAR*, const TCHAR*, const int, const int, const int, const int);
		//bool CheckNetfxVersionUsingMscoree(const TCHAR*);
		int GetNetfx10SPLevel();
		int GetNetfxSPLevel(const TCHAR*, const TCHAR*);
		DWORD GetProcessorArchitectureFlag();
		bool IsCurrentOSTabletMedCenter();
		bool IsNetfx10Installed();
		bool IsNetfx11Installed();
		bool IsNetfx20Installed();
		bool IsNetfx30Installed();
		bool IsNetfx35Installed();
		bool IsNetfx40ClientInstalled();
		bool IsNetfx40FullInstalled();
		bool IsNetfx45Installed();
		bool RegistryGetValue(HKEY, const TCHAR*, const TCHAR*, DWORD, LPBYTE, DWORD);

typedef enum {

    RUNTIME_INFO_UPGRADE_VERSION         = 0x01,
    RUNTIME_INFO_REQUEST_IA64            = 0x02,
    RUNTIME_INFO_REQUEST_AMD64           = 0x04,
    RUNTIME_INFO_REQUEST_X86             = 0x08,
    RUNTIME_INFO_DONT_RETURN_DIRECTORY   = 0x10,
    RUNTIME_INFO_DONT_RETURN_VERSION     = 0x20,
    RUNTIME_INFO_DONT_SHOW_ERROR_DIALOG  = 0x40

} RUNTIME_INFO_FLAGS;
*/
};
/*
static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
	{ wxCMD_LINE_SWITCH, "h", "help", "Displays help on the command line parameters",
		wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
	{ wxCMD_LINE_OPTION, "fv", "framework-version", "Runs silently, checking only for the .NET Framework version specified. Valid values are:1.0, 1.1, 2.0, 3.0, 3.5, 4.0client, 4.0full",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "sp", "service-pack", "Check for a specific service pack. Ignored if the fv parameter is not specified",
		wxCMD_LINE_VAL_NUMBER, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "et", "execute-true", "Full path to the executable to run if the version/service pack is found. Ignored if the fv parameter is not specified",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "ef", "execute-false", "Full path to the executable to run if the version/service pack is not found. Ignored if the fv parameter is not specified",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_NONE }
};
*/

static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
	{ wxCMD_LINE_SWITCH, "h", "help", "Displays help on the command line parameters",
		wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
	{ wxCMD_LINE_OPTION, "e", "exportpath", "The default location to save exported logs.",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_NONE }
};


DECLARE_APP(dotNetInspectorFrmApp)
#endif
