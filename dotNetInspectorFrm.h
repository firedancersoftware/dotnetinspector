///-----------------------------------------------------------------
///
/// @file      dotNetInspectorFrm.h
/// @author    Firedancer Software
/// Created:   6/05/2012 1:48:36 PM
/// @section   DESCRIPTION
///            dotNetInspectorFrm class declaration
///
///------------------------------------------------------------------

#ifndef __DOTNETINSPECTORFRM_H__
#define __DOTNETINSPECTORFRM_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/menu.h>
#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/sizer.h>
////Header Include End

////Dialog Style Start
#undef dotNetInspectorFrm_STYLE
#define dotNetInspectorFrm_STYLE wxCAPTION | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class dotNetInspectorFrm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();

	public:
		dotNetInspectorFrm(wxString defaultExportPath, wxWindow *parent = NULL, wxWindowID id = 1, const wxString &title = wxT("dotNetInspector"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = dotNetInspectorFrm_STYLE);
		virtual ~dotNetInspectorFrm();
//		void MnurefreshClick(wxCommandEvent& event);
		void MnusaveClick(wxCommandEvent& event);
		void MnuexitClick(wxCommandEvent& event);
		void MnucheckforupdatesClick(wxCommandEvent& event);
		void MnuwebsiteClick(wxCommandEvent& event);
		void MnuaboutClick(wxCommandEvent& event);
		void MnucopyTextClick(wxCommandEvent& event);
		void MnucopyHtmlClick(wxCommandEvent& event);

//		void WxButtonCopyTextClick(wxCommandEvent& event);
//		void WxButtonCopyHtmlClick(wxCommandEvent& event);

	    void OnTimer(wxTimerEvent& event);

		void OnCheckUpdateThread(wxCommandEvent& event);
		void CheckForUpdates(bool bCheckSilently);

	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxMenuBar *WxMenuBar1;
		wxStaticText *Wx45Label;
		wxStaticText *WxStaticText8;
		wxStaticText *Wx40fLabel;
		wxStaticText *WxStaticText7;
		wxStaticText *Wx40cLabel;
		wxStaticText *WxStaticText6;
		wxStaticText *Wx35Label;
		wxStaticText *WxStaticText5;
		wxStaticText *Wx30Label;
		wxStaticText *WxStaticText4;
		wxStaticText *Wx20Label;
		wxStaticText *WxStaticText3;
		wxStaticText *Wx11Label;
		wxStaticText *WxStaticText2;
		wxStaticText *Wx10Label;
		wxStaticText *WxStaticText1;
		wxFlexGridSizer *WxFlexGridSizer1;
		wxPanel *WxPanel4;
		wxBoxSizer *WxBoxSizer4;
//		wxButton *WxMoreButton;
//		wxButton *WxCopyTextButton;
//		wxButton *WxCopyHtmlButton;
		wxPanel *WxPanel3;
		wxBoxSizer *WxBoxSizer2;
		wxPanel *WxPanel1;
		wxBoxSizer *WxBoxSizer1;
		////GUI Control Declaration End

		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
//			ID_MNU_FILE = 1001,
//			ID_MNU_HELP = 1002,
			ID_MNU_CHECKFORUPDATES = 1001,
			ID_MNU_WEBSITE = 1002,
			ID_MNU_COPYTEXT = 1003,
			ID_MNU_COPYHTML = 1004,

			ID_WX45LABEL = 2058,
			ID_WXSTATICTEXT8 = 2057,
			ID_WX40FLABEL = 2056,
			ID_WXSTATICTEXT7 = 2055,
			ID_WX40CLABEL = 2054,
			ID_WXSTATICTEXT6 = 2053,
			ID_WX35LABEL = 2052,
			ID_WXSTATICTEXT5 = 2051,
			ID_WX30LABEL = 2050,
			ID_WXSTATICTEXT4 = 2049,
			ID_WX20LABEL = 2048,
			ID_WXSTATICTEXT3 = 2047,
			ID_WX11LABEL = 2046,
			ID_WXSTATICTEXT2 = 2045,
			ID_WX10LABEL = 2044,
			ID_WXSTATICTEXT1 = 2043,
			ID_WXPANEL3 = 2041,
			ID_WXPANEL4 = 2059,
			ID_WXREFRESHBUTTON = 2060,
			ID_WXMOREBUTTON = 2061,
			ID_WXCOPYTEXTBUTTON = 2062,
			ID_WXCOPYHTMLBUTTON = 2063,
			ID_WXPANEL1 = 2038,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
		void CheckFrameworkVersions();
		void SaveToFile();
		void CopyToClipboard(wxString text);

		wxString GetFormattedResults();
		wxString GetHtmlResults();

	    wxTimer *m_timer;

		bool bIsCheckingUpdates;

		bool bInstalled10;
		bool bInstalled11;
		bool bInstalled20;
		bool bInstalled30;
		bool bInstalled35;
		bool bInstalled40Client;
		bool bInstalled40Full;
		bool bInstalled45;

		long netFx10Sp;
		long netFx11Sp;
		long netFx20Sp;
		long netFx30Sp;
		long netFx35Sp;
		long netFx40ClientSp;
		long netFx40FullSp;
		long netFx45Sp;

		wxString netFx10Result;
		wxString netFx11Result;
		wxString netFx20Result;
		wxString netFx30Result;
		wxString netFx35Result;
		wxString netFx40ClientResult;
		wxString netFx40FullResult;
		wxString netFx45Result;

		wxString exportPath;

		bool CheckNetfxBuildNumber(const TCHAR*, const TCHAR*, const int, const int, const int, const int);
		//bool CheckNetfxVersionUsingMscoree(const TCHAR*);
		int GetNetfx10SPLevel();
		int GetNetfxSPLevel(const TCHAR*, const TCHAR*);
		DWORD GetProcessorArchitectureFlag();
		bool IsCurrentOSTabletMedCenter();
		bool IsNetfx10Installed();
		bool IsNetfx11Installed();
		bool IsNetfx20Installed();
		bool IsNetfx30Installed();
		bool IsNetfx35Installed();
		bool IsNetfx40ClientInstalled();
		bool IsNetfx40FullInstalled();
		bool IsNetfx45Installed();
		bool RegistryGetValue(HKEY, const TCHAR*, const TCHAR*, DWORD, LPBYTE, DWORD);

typedef enum {

    RUNTIME_INFO_UPGRADE_VERSION         = 0x01,
    RUNTIME_INFO_REQUEST_IA64            = 0x02,
    RUNTIME_INFO_REQUEST_AMD64           = 0x04,
    RUNTIME_INFO_REQUEST_X86             = 0x08,
    RUNTIME_INFO_DONT_RETURN_DIRECTORY   = 0x10,
    RUNTIME_INFO_DONT_RETURN_VERSION     = 0x20,
    RUNTIME_INFO_DONT_SHOW_ERROR_DIALOG  = 0x40

} RUNTIME_INFO_FLAGS;
};

#endif
